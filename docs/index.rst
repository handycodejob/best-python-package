.. tempconvert documentation master file, created by
   sphinx-quickstart on Tue May 22 00:50:18 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tempconvert's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: tempconvert
    :members:


.. automodule:: tempconvert.convert
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
